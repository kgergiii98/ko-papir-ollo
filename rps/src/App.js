import { useEffect, useState } from "react";
import "../src/App.css";

const App = () => {
  const [userChoice, setUserChoice] = useState(null);
  const [computerChoice, setComputerChoice] = useState(null);
  const [userChoicesSrc, setUserChoiceSrc] = useState(null);
  const [computerChoiceSrc, setComputerChoiceSrc] = useState(null);
  const [result, setResult] = useState(null);
  const choices = ["rock", "paper", "scissors"];

  const handleClick = (value) => {
    setUserChoice(value);
    setUserChoiceSrc(`./images/${value}.svg`);
    generateComputerChoice();
  };

  const reset = () => {
    setUserChoice(null);
    setComputerChoice(null);
    setResult(null);
  };

  const generateComputerChoice = () => {
    const randomChoice = choices[Math.floor(Math.random() * choices.length)];
    setComputerChoice(randomChoice);
  };

  useEffect(() => {
    {
      switch (userChoice + computerChoice) {
        case "scissorspaper":
        case "rockscissors":
        case "paperrock":
          setResult("YOU WIN!");
          break;
        case "paperscissors":
        case "scissorsrock":
        case "rockpaper":
          setResult("YOU LOSE!");
          break;
        case "rockrock":
        case "paperpaper":
        case "scissorsscissors":
          setResult("ITS A DRAW!");
          break;
      }
    }
  }, [computerChoice, userChoice]);

  return (
    <div className="container">
      <div className="rule">
        <h1>Rules:</h1>
        <p>
          Players start each round by saying, “rock, paper, scissors, shoot!” On
          “shoot,” each player holds out their fist for rock, flat hand for
          paper, or their index and middle finger for scissors. Rock crushes
          scissors, scissors cut paper, and paper covers rock. See who wins each
          round!
        </p>
      </div>
      <div className="game">
        <div className="result">
          <h1>{result}</h1>
        </div>
        <div className="choices">
          <h2>User : {userChoice}</h2>
          <img src={`./images/${userChoice}.svg`} />

          <img src={`./images/${computerChoice}.svg`} />
          <h2> {computerChoice} : Computer</h2>
        </div>
        <div className="rps">
          {choices.map((choice, index) => (
            <div>
              <button
                className="options"
                key={index}
                onClick={() => handleClick(choice)}
              >
                {choice}
              </button>
            </div>
          ))}
        </div>
        <button className="options" onClick={() => reset()}>RESET</button>
      </div>
    </div>
  );
};

export default App;
